﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RecipeMenu : MonoBehaviour
{
    public RecipePage recipePage;
    public Meal meal;
    public TextAsset recipeFile;

    void Start()
    {
        meal = new Meal(recipeFile, 3);
        recipePage.meal = meal;
        meal.needType[MealType.BREAD] = false;
        meal.needType[MealType.MAIN] = false;
        meal.needType[MealType.SIDE] = false;
        recipePage.SetMode(RecipePage.Mode.NAV);
        recipePage.SetCurrent(meal.recipePlans[meal.recipeNames[0]]);
    }

    public void returnToMenu() {
        SceneManager.LoadScene("MainMenu");
    }
}
