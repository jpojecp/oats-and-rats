﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlateModel {

    private Dictionary<string, Sprite> contentImages = new Dictionary<string, Sprite>();
    private Dictionary<MealType, bool> contents = new Dictionary<MealType, bool>();
    private int count = 0;
    private string spriteName;

    public int max = 1;

    public PlateModel() {
        clearMeal();

        Sprite[] sprites = Resources.LoadAll<Sprite>("platefill");
        foreach (Sprite s in sprites) {
            contentImages.Add(s.name, s);
        }
    }

    public void addMeal(MealType type) {
        if (count == max) { return; }
        count++;
        contents[type] = true;

        spriteName = "" + max;
        if (contents[MealType.BREAD]) {
            spriteName = spriteName + "bread";
        }
        if (contents[MealType.MAIN]) {
            spriteName = spriteName + "main";
        }
        if (contents[MealType.SIDE]) {
            spriteName = spriteName + "side";
        }
    }

    public Sprite getSprite() {
        if (string.IsNullOrEmpty(spriteName)) {
            return null;
        } else {
            return contentImages[spriteName];
        }
    }

    public bool isFull() {
        return count >= max;
    }

    public bool isEmpty() {
        return count == 0;
    }
    
    public bool has(MealType type) {
        return contents[type];
    }

    public Dictionary<MealType, bool> clearMeal() {
        Dictionary<MealType, bool> result = new Dictionary<MealType, bool>(contents);

        contents[MealType.BREAD] = false;
        contents[MealType.MAIN] = false;
        contents[MealType.SIDE] = false;
        spriteName = null;
        count = 0;

        return result;
    }

}