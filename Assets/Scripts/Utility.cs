﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Shuffler {

    public static void Shuffle<T>(this List<T> list) {
        Random rand = new Random();
        int n = list.Count();

        while (n > 1) {
            n--;
            int k = rand.Next(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}

public enum ForagePlace { BUSH, ROOT, TREE, WATER, FARM }