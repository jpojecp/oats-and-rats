﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    private Rigidbody2D body;
    private float horiz;
    private float vert;
    private InteractionTrigger interactTarget = null;
    private bool walking = false;
    private int blinkTimer = 1000;
    private bool left = true;
    private Flippable flip;
    private bool isSlowed = false;
    private List<InteractionTrigger> triggerList = new List<InteractionTrigger>();

    public float speed = 4.0f;
    public InventoryController inventory;
    public bool running = false;
    public GameObject conveyance;
    public Animator animHead;
    public Animator animLegs;
    public AudioClip soundPlate;
    public AudioClip soundBush;
    public AudioClip soundSplash;

    void Start() {
        body = GetComponent<Rigidbody2D>();
        flip = GetComponent<Flippable>();
    }

    void Update() {
        float oldHoriz = horiz;
        horiz = Input.GetAxis("Horizontal");
        vert = Input.GetAxis("Vertical");

        if (Input.GetKeyDown("f") && running) {
            foreach (InteractionTrigger t in triggerList) {
                if (t.Interactable()) {
                    t.Interact();
                    break;
                }
            }
        }
        

        if (Input.GetKeyDown("q") && running) {
            string item = LevelController.Level().inventory.pockets[0];
            LevelController.Level().inventory.removeItem(item);
            if (interactTarget != null) {
                interactTarget.PrintMsg();
            }
        }

        if (conveyance.activeSelf && running && (horiz != 0 || vert != 0)) {
            Animator cAnim = conveyance.GetComponent<Animator>();
            cAnim.SetTrigger("fade");
            float length = cAnim.GetCurrentAnimatorStateInfo(0).length / 2;
            StartCoroutine(disableConveyance(length));
        }

        if (left && horiz > 0) {
            flip.Flip();
            left = false;
        } else if (!left && horiz < 0) {
            flip.Flip();
            left = true;
        }

        if (!walking && (!Mathf.Approximately(horiz, 0) || !Mathf.Approximately(vert, 0))) {
            animLegs.SetBool("walking", true);
            walking = true;
        } else if (walking && Mathf.Approximately(horiz, 0) && Mathf.Approximately(vert, 0)) {
            animLegs.SetBool("walking", false);
            walking = false;
        }

        blinkTimer--;
        if (blinkTimer <= 0) {
            blinkTimer = Random.Range(400, 2000);
            animHead.SetTrigger("blink");
        }
    }

    public void OnTriggerStay2D(Collider2D collision) {
        if (collision.CompareTag("Slow")) {
            isSlowed = true;
        }
    }

    private void FixedUpdate() {
        float diag = speed;
        if (horiz != 0f && vert != 0f) {
            diag = speed * 0.8f;
        }
        if (isSlowed) {
            diag = diag * 0.5f;
        }
        if (running) {
            body.velocity = new Vector2(horiz * diag, vert * diag);
        } else {
            body.velocity = Vector2.zero;
        }
        isSlowed = false;
    }

    public void addTrigger(InteractionTrigger t) {
        if (!triggerList.Contains(t)) {
            triggerList.Add(t);
        }
        printTrigger();
    }

    public void removeTrigger(InteractionTrigger t) {
        triggerList.Remove(t);
        printTrigger();
    }

    public void setRunning(bool val) {
        running = val;
        if (!val) {
            horiz = 0;
            vert = 0;
            body.velocity = new Vector2(0, 0);
        }
    }

    public IEnumerator disableConveyance(float length) {
        yield return new WaitForSeconds(length);
        conveyance.SetActive(false);
    }

    public void printTrigger() {
        InteractionTrigger result = null;
        foreach(InteractionTrigger t in triggerList) {
            if (t.Interactable()) {
                result = t;
            }
        }
        if (result != null) {
            result.PrintMsg();
        } else {
            LevelController.Level().messageBar.PrintMsg("", null);
        }
    }

    public void playSound(string type) {
        if (type == "bush") {
            GetComponent<AudioSource>().PlayOneShot(soundBush);
        } else if (type == "splash") {
            GetComponent<AudioSource>().PlayOneShot(soundSplash);
        } else if (type == "plate") {
            GetComponent<AudioSource>().PlayOneShot(soundPlate);
        }
    }
}
