﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flippable : MonoBehaviour
{
    public bool flipX = false;

    private void Start() {
        if (flipX) {
            Flip();
        }
    }
    public void Flip() {
        foreach (SpriteRenderer s in GetComponentsInChildren<SpriteRenderer>()) {
            s.flipX = !s.flipX;
            flipX = !flipX;
        }
    }
}
