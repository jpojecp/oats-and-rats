﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoupPotPage : InteractiveMinigame {

    public Image imgRotate;
    public Image imgLeftHammock;
    public Image imgRightHammock;

    [Space(10)]
    public SoupButton wineButton;
    public SoupButton spiceButton;
    public SoupButton zukeButton;
    public SoupButton onionButton;
    public SoupButton pepperButton;
    public SoupButton cheeseButton;

    [Space(10)]
    public AudioClip soundSplash;

    private List<SoupButton> buttons = new List<SoupButton>();
    private SoupButton current;
    private float balance = 0f;

    public override void pseudoStart() {
        displayBar();

        buttons.AddRange(new SoupButton[]{ wineButton, spiceButton, zukeButton, onionButton,
            pepperButton, cheeseButton});

        wineButton.up = zukeButton;
        wineButton.down = spiceButton;
        wineButton.side = onionButton;

        spiceButton.up = wineButton;
        spiceButton.down = zukeButton;
        spiceButton.side = pepperButton;

        zukeButton.up = spiceButton;
        zukeButton.down = wineButton;
        zukeButton.side = cheeseButton;

        onionButton.up = cheeseButton;
        onionButton.down = pepperButton;
        onionButton.side = wineButton;

        pepperButton.up = onionButton;
        pepperButton.down = cheeseButton;
        pepperButton.side = spiceButton;

        cheeseButton.up = pepperButton;
        cheeseButton.down = onionButton;
        cheeseButton.side = zukeButton;

        foreach (SoupButton b in buttons) {
            b.SetSelected(false);
        }
    }

    public override void BeginGame() {
        base.BeginGame();
        if (tutorial) {
            tutorial = false;
            return;
        }

        if (current != null) {
            current.SetSelected(false);
        }
        current = buttons[Random.Range(0, 6)];
        current.SetSelected(true);

        for (int i = 0; i < 3; i++) {
            buttons[i].val = true;
        }
        buttons[Random.Range(0, 3)].val = false;
        for (int i = 3; i < 6; i++) {
            buttons[i].val = false;
        }
        buttons[Random.Range(3, 6)].val = true;

        while (balance < 0) {
            Rotate(true);
        }
        while (balance > 0) {
            Rotate(false);
        }

        GetComponent<AudioSource>().Play();
    }

    public override void keyUp() {
        if (tutorial || !running) { return; }
        ChangeButton(current.up);
    }

    public override void keyDown() {
        if (tutorial || !running) { return; }
        ChangeButton(current.down);
    }

    public override void keyLeft() {
        if (tutorial || !running) { return; }
        ChangeButton(current.side);
    }

    public override void keyRight() {
        if (tutorial || !running) { return; }
        ChangeButton(current.side);
    }

    public void ChangeButton(SoupButton next) {
        current.SetSelected(false);
        current = next;
        current.SetSelected(true);
        Rotate(current.val);
        GetComponent<AudioSource>().PlayOneShot(soundSplash);
    }

    public void Rotate(bool dir) {
        float angle = (dir ? 10f : -10f);
        float opposite = -angle;

        imgRotate.transform.Rotate(new Vector3(0, 0, angle));
        imgLeftHammock.transform.Rotate(new Vector3(0, 0, opposite));
        imgRightHammock.transform.Rotate(new Vector3(0, 0, opposite));

        balance += angle;
        if (Mathf.Approximately(balance, 0)) {
            increase();
        } else {
            if (Mathf.Abs(balance) >= 40f) {
                StartLose();
            }
        }
    }
}
