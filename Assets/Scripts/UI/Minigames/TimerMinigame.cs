﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class TimerMinigame : Minigame {
    public GameObject imgTimer;
    public float rot = 0;

    private void Start() {
        pause = false;
    }

    public override void BeginGame() {
        rot = 0;
        imgTimer.transform.rotation = Quaternion.identity;
        gameObject.SetActive(true);
    }

    public void FixedUpdate() {
        rot += Time.deltaTime * -20;
        imgTimer.transform.Rotate(Vector3.forward * Time.deltaTime * -20);

        if (rot <= -360) {
            Win();
        }
    }

    public override void Win() {
        gameObject.SetActive(false);
        onWin.Invoke();
    }
}