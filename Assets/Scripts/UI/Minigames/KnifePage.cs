﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnifePage : InteractiveMinigame {

    public Image imgWholeCarrot;
    public Image imgKnife;
    public Image imgChunk;
    public Image imgCrumbs;
    public Image imgSlice;
    public GameObject knifeSheath;
    public GameObject carrotSheath;
    public Mask maskCarrot;
    public float speed = 0.0f;
    public AudioClip soundChop;

    private SpriteCutter cutter;
    private float slideDistance = 0f;
    private float knifeDistance = 0f;
    private float totalDistance = 0f;
    private Animator knifeAnim;
    private Animator crumbAnim;
    private Animator sliceAnim;
    private Animator chunkAnim;

    public override void pseudoStart() {
        displayBar();

        cutter = maskCarrot.GetComponent<SpriteCutter>();
        knifeAnim = imgKnife.GetComponent<Animator>();
        crumbAnim = imgCrumbs.GetComponent<Animator>();
        sliceAnim = imgSlice.GetComponent<Animator>();
        chunkAnim = imgChunk.GetComponent<Animator>();
    }

    public override void BeginGame() {
        base.BeginGame();
        if (tutorial) {
            tutorial = false;
            return;
        }

        slideDistance = 0f;
        knifeDistance = 0f;
        totalDistance = 0f;
        cutter.total = 0f;

        imgWholeCarrot.transform.localPosition = Vector3.zero;
        knifeSheath.transform.localPosition = Vector3.zero;
        carrotSheath.transform.localPosition = Vector3.zero;
        maskCarrot.transform.localPosition = Vector3.zero;
    }

    public override void Update() {
        base.Update();

        if (tutorial || !running) { return; }
        MoveCarrot();
    }

    public override void keyUp() {
        if (tutorial || !running) { return; }
        keyDown();
    }

    public override void keyDown() {
        if (tutorial || !running) { return; }
        knifeAnim.SetTrigger("down");
        float cutSize = slideDistance + knifeDistance;
        if (cutSize > 0) {
            cutter.Cut(cutSize);
            slideDistance = 0f;
            knifeDistance = 0f;

            if (cutSize < 30) {
                crumbAnim.SetTrigger("particle");
            } else if (cutSize > 50) {
                chunkAnim.SetTrigger("particle");
            } else {
                increase();
                sliceAnim.SetTrigger("particle");
            }

            GetComponent<AudioSource>().PlayOneShot(soundChop);

            if (cutter.Finished()) {
                StartLose();
            }
        }
    }

    public override void keyLeft() {
        if (tutorial || !running) { return; }
        knifeDistance += 10f;
        knifeSheath.transform.localPosition -= new Vector3(10f, 0, 0);
    }

    public override void keyRight() {
        if (tutorial || !running) { return; }
        knifeDistance -= 10f;
        knifeSheath.transform.localPosition += new Vector3(10f, 0, 0);
    }

    private void MoveCarrot() {
        slideDistance += speed;
        totalDistance += speed;
        carrotSheath.transform.localPosition += new Vector3(speed, 0, 0);

        if (totalDistance > imgWholeCarrot.rectTransform.rect.width) {
            StartLose();
        }
    }
}
