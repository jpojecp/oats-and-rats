﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoupButton : MonoBehaviour
{
    public Image imgSelected;
    public SoupButton up;
    public SoupButton down;
    public SoupButton side;
    public bool val;
    public void SetSelected(bool val) {
        imgSelected.gameObject.SetActive(val);
    }
}
