﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class Minigame : MonoBehaviour {

    public UnityEvent onWin;
    public UnityEvent onLose;
    public bool pause = true;

    public virtual void BeginGame() {

    }

    public virtual void Win() {
    }

    public virtual void Lose() {
    }
}