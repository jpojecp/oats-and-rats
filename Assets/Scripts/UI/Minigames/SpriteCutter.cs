﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteCutter : MonoBehaviour
{
    public Image img;

    public float total = 0f;

    public void Cut(float cutSize) {
        if (total < img.rectTransform.rect.width) {
            total += cutSize;
            transform.localPosition -= new Vector3(cutSize, 0, 0);
            img.transform.localPosition += new Vector3(cutSize, 0, 0);
        }
    }

    public bool Finished() {
        return total + 130f > img.rectTransform.rect.width;
    }
}
