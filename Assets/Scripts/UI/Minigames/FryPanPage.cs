﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FryPanPage : InteractiveMinigame {

    public GameObject sheath;
    public Image imgFish;
    public Image imgGrease;

    private Animator animSheath;
    private float timerStick = 0f;
    private float timerBurn = 0f;

    public override void pseudoStart() {
        displayBar();
        animSheath = sheath.GetComponent<Animator>();
    }

    public override void BeginGame() {
        base.BeginGame();
        if (tutorial) {
            tutorial = false;
            return;
        }

        timerStick = 0f;
        timerBurn = 0f;

        GetComponent<AudioSource>().Play();
    }

    public override void Update() {
        base.Update();

        if (!running) { return; }

        timerStick += 0.003f;
        timerBurn += 0.001f;

        if (timerBurn > 1) {
            StartLose();
        }

        imgFish.color = new Color(1 - timerBurn, 1 - timerBurn, 1 - timerBurn, 1);
        imgGrease.color = new Color(1, 1, 1, timerStick);
    }

    public override void keyUp() {
        if (tutorial || !running) { return; }
        Flip();
    }

    public override void keyDown() {
        if (tutorial || !running) { return; }
        Flip();
    }

    public override void keyLeft() {
        if (tutorial || !running) { return; }
        Shake();
    }

    public override void keyRight() {
        if (tutorial || !running) { return; }
        Shake();
    }

    public void Shake() {
        if (animSheath.GetCurrentAnimatorStateInfo(0).IsName("frypanidle")) {
            timerStick = 0f;
            animSheath.SetTrigger("shake");
        }
    }

    public void Flip() {
        if (animSheath.GetCurrentAnimatorStateInfo(0).IsName("frypanidle")) {
            if (timerStick >= 1) {
                animSheath.SetTrigger("stick");
            } else {
                increase();
                timerBurn = 0f;
                animSheath.SetTrigger("flip");
            }
        }
    }
}
