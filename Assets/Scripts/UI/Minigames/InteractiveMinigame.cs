﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class InteractiveMinigame : Minigame {

    public TextMeshProUGUI txtWin;
    public TextMeshProUGUI txtLose;

    public Image bar_background;
    public Image bar;
    public GameObject contentDetails;
    public GameObject tutorialDetails;

    public int val = 0;
    public int max = 5;
    public bool running = true;
    public bool tutorial = false;

    private bool started = false;

    public override void BeginGame() {
        if (tutorial) {
            tutorialDetails.gameObject.SetActive(true);
            contentDetails.gameObject.SetActive(false);
        } else {
            tutorialDetails.gameObject.SetActive(false);
            contentDetails.SetActive(true);
            if (!started) {
                pseudoStart();
                started = true;
            }
            val = 0;
            displayBar();
            running = true;
        }
        txtWin.gameObject.SetActive(false);
        txtLose.gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public virtual void Update() {
        if (!running) { return; }
        if (Input.GetKeyDown("a") || Input.GetKeyDown("left")) {
            keyLeft();
        } else if (Input.GetKeyDown("d") || Input.GetKeyDown("right")) {
            keyRight();
        } else if (Input.GetKeyDown("w") || Input.GetKeyDown("up")) {
            keyUp();
        } else if (Input.GetKeyDown("s") || Input.GetKeyDown("down")) {
            keyDown();
        }
    }

    public virtual void pseudoStart() {

    }

    public virtual void keyUp() {
        
    }

    public virtual void keyDown() {

    }

    public virtual void keyLeft() {

    }

    public virtual void keyRight() {

    }

    public override void Win() {
        GetComponent<AudioSource>().Stop();
        gameObject.SetActive(false);
        onWin.Invoke();
    }

    public override void Lose() {
        GetComponent<AudioSource>().Stop();
        gameObject.SetActive(false);
        onLose.Invoke();
    }

    public IEnumerator DelayWin() {
        yield return new WaitForSeconds(2);
        Win();
    }

    public IEnumerator DelayLose() {
        yield return new WaitForSeconds(2);
        Lose();
    }

    public void StartWin() {
        running = false;
        txtWin.gameObject.SetActive(true);
        StartCoroutine(DelayWin());
    }

    public void StartLose() {
        running = false;
        txtLose.gameObject.SetActive(true);
        StartCoroutine(DelayLose());
    }

    public void increase() {
        if (!running) { return; }
        val++;
        displayBar();
        if (val >= max) {
            StartWin();
        }
    }

    public void displayBar() {
        float length;
        if (val == 0) {
            length = 0f;
        } else {
            length = (bar_background.rectTransform.rect.width * val) / max;
        }
        bar.rectTransform.sizeDelta = new Vector2(length, bar.rectTransform.rect.height);
    }
}
