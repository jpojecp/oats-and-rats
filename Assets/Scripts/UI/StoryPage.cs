﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class StoryPage : MonoBehaviour {

    public Text txt;
    public TextMeshProUGUI txtTitle;
    public Text txtSubtitle;
    public TextAsset storyFile;
    public Objectives obj;

    private List<string> content;
    private int index = 0;

    private void Start() {
        string[] lines = storyFile.text.Split('\n');
        content = new List<string>(lines);
    }

    void Update() {
        if (Input.anyKeyDown
            || Input.GetMouseButtonDown(0)
            || Input.GetMouseButtonDown(1)
            || Input.GetMouseButtonDown(2)) {

            if (txtTitle.gameObject.activeSelf) {
                txtTitle.gameObject.SetActive(false);
                txtSubtitle.gameObject.SetActive(false);
                obj.gameObject.SetActive(false);
                txt.text = content[0];
            } else {
                index++;
                if (index < content.Count) {
                    txt.text = content[index];
                } else {
                    CloseStory();
                }
            }
        }
    }

    public void CloseStory() {
        gameObject.SetActive(false);
        if (!string.IsNullOrEmpty(LevelController.Level().singleRecipe)) {
            LevelController.Level().OpenRecipes();
        } else {
            LevelController.Level().Unpause();
        }
    }
}
