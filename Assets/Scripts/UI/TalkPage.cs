﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkPage : MonoBehaviour
{
    public DelegateController character;
    public Text txtName;
    public Text txtType;
    public Text txtLike;
    public Button btnDelegate;

    public void SetCharacter(DelegateController character) {
        this.character = character;

        if (LevelController.Level().canDelegate && character.canDelegate) {
            btnDelegate.interactable = true;
        } else {
            btnDelegate.interactable = false;
        }

        txtName.text = "Name: " + character.characterName;
        txtType.text = "Animal: " + character.animalType;
        txtLike.text = ": " + character.favRecipe;
    }

    public void Delegate() {
        character.Delegate();
        gameObject.SetActive(false);
        LevelController.Level().Unpause();
    }
}
