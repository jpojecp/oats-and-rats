﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;
using System.Linq;

public class VictoryPage : MonoBehaviour
{
    public TextMeshProUGUI txtTitle;
    public Text txtDesc;
    public string nextScene;
    public Button btnContinue;
    public CountdownController countdown;
    public AudioClip winMusic;
    public AudioClip loseMusic;
    
    public void Replay() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Continue() {
        SceneManager.LoadScene(nextScene);
    }

    public void ExitGame() {
        Application.Quit();
    }

    public void victory(string title, string description, bool win) {
        txtTitle.text = title;
        txtDesc.text = description;
        if (!win) {
            btnContinue.interactable = false;
        }
        countdown.stopCountdown();

        if (MusicController.music() != null) {
            MusicController.music().fadeSong();
        }
        

        // bleh this doesn't take into account meal types

        if (win && !LevelController.Level().tutorial && string.IsNullOrEmpty(LevelController.Level().singleRecipe)) {

            List<string> liked = new List<string>();

            foreach(KeyValuePair<string, int> pair in LevelController.Level().meal.recipeLikes) {
                for (int i = 0; i < pair.Value; i++) {
                    liked.Add(pair.Key);
                }
            }
            List<string> cooked = LevelController.Level().meal.cookedNames;

            int actualLiked = liked.Intersect(cooked).Count();
            if (actualLiked == 1) {
                txtDesc.text = " 1 animal loved their meal";
            } else {
                txtDesc.text = "" + actualLiked + " animals loved their meal";
            }

        }
    }

    public void victoryMusic(bool win) {
        AudioSource audio = GetComponent<AudioSource>();
        if (win) {
            audio.PlayOneShot(winMusic);
        } else {
            audio.PlayOneShot(loseMusic);
        }
    }
}
