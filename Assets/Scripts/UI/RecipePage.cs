﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class RecipePage : MonoBehaviour {

    public enum Mode {
        NAV, CANCEL, SINGLE, CHOOSE
    }

    public Mode mode;
    public Meal meal;

    [Space(10)]
    public TextMeshProUGUI txtTitle;
    public GameObject recipeList;
    public Text txtRecipeItem;
    public Text txtHeart;
    public Text txtPlate;
    public Text txtStation;
    public Text txtType;

    [Space(10)]
    public Button btnBack;
    public Button btnForward;
    public Button btnChoose;
    public Button btnCancel;

    [Space(10)]
    public Image imgBread;
    public Image imgMain;
    public Image imgSide;

    [Space(10)]
    public Image imgFrypan;
    public Image imgKnife;
    public Image imgOven;
    public Image imgSoupPot;

    [Space(10)]
    public Sprite enabledBG;
    public Sprite disabledBG;
    public Image bgImage;
    public Image imgFood;

    [Space(10)]
    public CookingStation currentStation;

    private Meal.Recipe currentRecipe;
    private int recipeIndex = 0;
    private Dictionary<Station, Image> stationImages = new Dictionary<Station, Image>();

    private void Update() {
        if (gameObject.activeSelf && btnChoose.interactable && Input.GetKeyDown("f")) {
            Choose();
        }

        if (gameObject.activeSelf && Input.GetKeyDown("escape")) {
            gameObject.SetActive(false);
            LevelController.Level().Unpause();
        }
    }

    public Meal.Recipe GetCurrent() {
        return currentRecipe;
    }

    public void Choose() {
        gameObject.SetActive(false);
        currentStation.startMinigame(currentRecipe);
    }

    public void SetCurrent(Meal.Recipe currentRecipe) {
        recipeIndex = meal.recipeNames.IndexOf(currentRecipe.name);
        this.currentRecipe = currentRecipe;
        txtTitle.text = currentRecipe.name;
        SetMealImage(currentRecipe.type);
        setStationImage(currentRecipe.station);
        setFoodImage(currentRecipe.name);
        setBackground(currentRecipe.hasAll());
        setHeart(meal.recipeLikes[currentRecipe.name]);
        setPlate(meal.platedAmountByType[currentRecipe.type]);

        // recipe list
        foreach (Transform child in recipeList.transform) {
                Destroy(child.gameObject);
        }
        foreach (KeyValuePair<string, bool> item in currentRecipe.actual) {
            Text t = Instantiate(txtRecipeItem, recipeList.transform);
            t.text = "- " + item.Key;
            if (LevelController.Level() != null && !LevelController.Level().inventory.pockets.Contains(item.Key)) {
                t.color = Color.red;
            }
        }

        // which navs are disabled
        if (LevelController.Level() != null && !string.IsNullOrEmpty(LevelController.Level().singleRecipe)) {
            btnBack.interactable = false;
            btnForward.interactable = false;
        } else {
            if (recipeIndex == 0) {
                btnBack.interactable = false;
            } else {
                btnBack.interactable = true;
            }

            if (recipeIndex == meal.recipeNames.Count - 1) {
                btnForward.interactable = false;
            } else {
                btnForward.interactable = true;
            }
        }

        // enable choose button
        if (mode == Mode.CHOOSE && currentRecipe.hasAll()
            && currentRecipe.station == currentStation.stationType) {
            btnChoose.interactable = true;
        } else {
            btnChoose.interactable = false;
        }
    }

    public void SetMode(Mode mode) {
        this.mode = mode;

        // revamp:
        // nav and choose are the only ones I actually need
        // technically there's a use for single, but I can detect
        // that when needed from level

        switch (mode) {
            case Mode.CANCEL:
                // only enable cancel button
                btnBack.interactable = false;
                btnForward.interactable = false;
                btnChoose.interactable = false;
                break;
            case Mode.CHOOSE:
                // enable cancel and dynamic choose
                btnBack.interactable = true;
                btnForward.interactable = true;
                btnChoose.gameObject.SetActive(true);
                btnChoose.interactable = true;
                break;
            case Mode.NAV:
                // navigation and cancel
                btnBack.interactable = true;
                btnForward.interactable = true;
                btnChoose.interactable = false;
                btnChoose.gameObject.SetActive(false);
                break;
            case Mode.SINGLE:
                // only enable choose
                // no navigation
                btnBack.interactable = false;
                btnForward.interactable = false;
                btnChoose.interactable = false;
                break;
        }
    }

    public void Back() {
        Meal.Recipe rec = null;

        if (recipeIndex == 0) {
            // do nothing, start of list
        } else {
            recipeIndex--;
            rec = meal.recipePlans[meal.recipeNames[recipeIndex]];
        }

        if (rec != null) {
            SetCurrent(rec);
        }
    }

    public void Forward() {
        Meal.Recipe rec = null;

        if (recipeIndex == meal.recipeNames.Count - 1) {
            // do nothing, end of list
        } else {
            recipeIndex++;
            rec = meal.recipePlans[meal.recipeNames[recipeIndex]];
        }

        if (rec != null) {
            SetCurrent(rec);
        }
    }

    public void SetMealImage(MealType type) {
        imgBread.gameObject.SetActive(false);
        imgMain.gameObject.SetActive(false);
        imgSide.gameObject.SetActive(false);
        switch (type) {
            case MealType.BREAD:
                imgBread.gameObject.SetActive(true);
                break;
            case MealType.MAIN:
                imgMain.gameObject.SetActive(true);
                break;
            case MealType.SIDE:
                imgSide.gameObject.SetActive(true);
                break;
        }

        txtType.text = "Type: " + type.ToString();
    }

    public void setStationImage(Station type) {
        imgFrypan.gameObject.SetActive(false);
        imgKnife.gameObject.SetActive(false);
        imgOven.gameObject.SetActive(false);
        imgSoupPot.gameObject.SetActive(false);

        if (stationImages.Count == 0) {
            stationImages[Station.FRYPAN] = imgFrypan;
            stationImages[Station.KNIFE] = imgKnife;
            stationImages[Station.OVEN] = imgOven;
            stationImages[Station.SOUPPOT] = imgSoupPot;
        }

        stationImages[type].gameObject.SetActive(true);
        txtStation.text = "Station: " + type.ToString();
    }

    public void setFoodImage(string food) {
        Resources.UnloadAsset(imgFood.sprite);
        Sprite s = Resources.Load<Sprite>("bigfood/" + food);
        imgFood.sprite = s;
    }

    public void setBackground(bool enabled) {
        if (enabled) {
            bgImage.sprite = enabledBG;
        } else {
            bgImage.sprite = disabledBG;
        }
    }

    public void setHeart(int amount) {
        if (amount == 1) {
            txtHeart.text = "1 animal likes this";
        } else {
            txtHeart.text = "" + amount + " animals like this";
        }
        
    }

    public void setPlate(int amountCooked) {
        if (LevelController.Level() == null) {
            txtPlate.text = "";
        } else {
            txtPlate.text = "" + amountCooked + "/" + LevelController.Level().mouths;
        }
    }
}
