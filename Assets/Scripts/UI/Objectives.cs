﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objectives : MonoBehaviour
{
    public Text txtPlates;
    public Image imgBread;
    public Image imgMain;
    public Image imgSide;

    void Start()
    {
        if (LevelController.Level().tutorial) {
            gameObject.SetActive(false);
        } else {
            txtPlates.text = "x" + LevelController.Level().mouths;
            imgBread.gameObject.SetActive(LevelController.Level().needBread);
            imgMain.gameObject.SetActive(LevelController.Level().needMain);
            imgSide.gameObject.SetActive(LevelController.Level().needSide);
        }
    }
}
