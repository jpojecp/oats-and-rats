﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainMenu : MonoBehaviour {

    public AudioClip clip;

    private void Start() {
        if (MusicController.music() != null) {
            MusicController.music().changeSong(clip);
        }
    }

    public void btnNewGame() {
        SceneManager.LoadScene("OatHill1");
    }

    public void btnRecipes() {

    }

    public void btnCredits() {
        SceneManager.LoadScene("Credits");
    }

    public void btnExit() {
        Application.Quit();
    }

    public void loadLevel(string name) {
        SceneManager.LoadScene(name);
    }
}
