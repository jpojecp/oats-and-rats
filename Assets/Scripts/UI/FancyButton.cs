﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class FancyButton : MonoBehaviour
{
    public enum ButtonType { YELLOW, GREEN }
    public ButtonType type;
    public string text;

    void Awake() {
        StyleButton();
    }

    void Update() {
        StyleButton();
    }

    void StyleButton() {
        GetComponentInChildren<Text>().text = text;

        Sprite bg = null;
        switch (type) {
            case ButtonType.YELLOW:
                bg = Resources.Load<Sprite>("buttonYellow");
                break;
            case ButtonType.GREEN:
                bg = Resources.Load<Sprite>("buttonGreen");
                break;
        }
        GetComponent<Image>().sprite = bg;

        SpriteState ss = new SpriteState();
        ss.pressedSprite = bg;
        ss.selectedSprite = bg;
        ss.highlightedSprite = Resources.Load<Sprite>("buttonWhite");
        ss.disabledSprite = Resources.Load<Sprite>("buttonDisabled");
        GetComponent<Button>().spriteState = ss;
    }
}
