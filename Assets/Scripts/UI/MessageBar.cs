﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBar : MonoBehaviour
{

    public Text txtInfo;
    public Image imgTextBG;
    public Image imgIcon;
    // Start is called before the first frame update
    public void PrintMsg(string msg, string icon) {
        txtInfo.text = msg;
        if (msg == "") {
            imgTextBG.gameObject.SetActive(false);
        } else {
            imgTextBG.gameObject.SetActive(true);
        }

        if (icon == null) {
            imgIcon.gameObject.SetActive(false);
        } else {
            imgIcon.sprite = LevelController.Level().foodImages[icon];
            imgIcon.gameObject.SetActive(true);
        }
    }
}
