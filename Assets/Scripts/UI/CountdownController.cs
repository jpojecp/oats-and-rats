﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownController : MonoBehaviour {

    private float countdown;
    private float minutes;
    private float seconds;

    public int  timer = 2;
    public bool running = true;
    public Text text;

    public void startCountdown()
    {
        running = true;
        countdown = (timer * 60) + 1;
        UnpauseCountdown();
    }

    public void stopCountdown() {
        running = false;
        minutes = 0;
        seconds = 0;
        PauseCountdown();
    }

    public void PauseCountdown() {
        StopCoroutine(updateCoroutine());
    }

    public void UnpauseCountdown() {
        StartCoroutine(updateCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        if (!running) return;
        countdown -= Time.deltaTime;

        minutes = Mathf.Floor(countdown / 60);
        seconds = countdown % 60;
        if (seconds > 59) seconds = 59;
        if (minutes < 0) {
            stopCountdown();
            LevelController.Level().victory("TIME'S UP!", "You didn't finish in time", false);
        }
    }

    private IEnumerator updateCoroutine()
    {
        while (running) {
            text.text = string.Format("{0:0}:{1:00}", minutes, seconds);
            yield return new WaitForSeconds(0.2f);
        }
    }

}
