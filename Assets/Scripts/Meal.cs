﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public enum Station {
    SOUPPOT, FRYPAN, KNIFE, OVEN
}

public enum MealType {
    MAIN, SIDE, BREAD
}

public class Meal {
    
    public class Recipe : System.ICloneable {
        public string name;
        public List<string> plan;
        public Dictionary<string, bool> actual;
        public Station station;
        public MealType type;
        public bool cooked = false;

        public Recipe(string name, List<string> plan, Station station, MealType type) {
            this.name = name;
            this.plan = plan;
            this.station = station;
            this.type = type;

            actual = new Dictionary<string, bool>();
            foreach (string p in plan) {
                actual[p] = false;
            }
        }

        public void addItem(string item) {
            actual[item] = true;
        }

        public bool hasAll() {
            if (LevelController.Level() == null) {
                return false;
            }
            return LevelController.Level().inventory.pockets.Intersect(plan).Count() == plan.Count();
        }

        public object Clone() {
            return new Recipe(name, new List<string>(plan), station, type);
        }

        public bool hasItems(List<string> items) {
            bool result = plan.Any(x => items.Any(y => x == y));

            return result;
        }

        public int countItems(List<string> items) {
            return plan.Intersect(items).Count();
        }
    }

    public Dictionary<string, Recipe> recipePlans = new Dictionary<string, Recipe>();
    public Dictionary<Station, List<Recipe>> recipePlansByStation = new Dictionary<Station, List<Recipe>>();
    public List<string> recipeNames = new List<string>();
    public List<string> cookedNames = new List<string>();
    public Dictionary<MealType, int> platedAmountByType = new Dictionary<MealType, int>();
    public Dictionary<string, int> recipeLikes = new Dictionary<string, int>();
    public int mealLevel;
    public int mouths = 0;
    public Dictionary<MealType, bool> needType = new Dictionary<MealType, bool>();

    public Meal(TextAsset recipeFile, int mealLevel) {
        this.mealLevel = mealLevel;

        foreach (Station s in Enum.GetValues((typeof(Station)))) {
            recipePlansByStation[s] = new List<Recipe>();
        }

        foreach (MealType t in Enum.GetValues((typeof(MealType)))) {
            platedAmountByType[t] = 0;
        }

        ReadFile(recipeFile);
    }

    public void ReadFile(TextAsset recipeFile) {
        string[] lines = recipeFile.text.Split('\n');
        string[] allIngredients = lines[0].Split(',');

        // skip first few lines
        for (int i = 4; i < lines.Length; i++) {
            string[] line = lines[i].Split(',');

            string name = line[0];
            MealType type = (MealType)System.Enum.Parse(typeof(MealType), line[1]);
            Station station = (Station)System.Enum.Parse(typeof(Station), line[2]);
            int mLevel = int.Parse(line[3]);

            List<string> plan = new List<string>();

            for (int j = 5; j < line.Length; j++) {
                if (line[j] == "1") {
                    plan.Add(allIngredients[j]);
                }
            }

            // TODO FOODLEVEL
            if (mLevel <= mealLevel) {
                Recipe rec = new Recipe(name, plan, station, type);
                recipeNames.Add(rec.name);
                recipePlans[rec.name] = rec;
                recipePlansByStation[rec.station].Add(rec);
                recipeLikes[rec.name] = 0;
            }
        }
    }

    public void cookRecipe(string name) {

    }
}
