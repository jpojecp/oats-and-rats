﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishTrigger : ForageTrigger
{
    private int countdown = 100;
    private Animator anim;

    private void Start() {
        anim = GetComponent<Animator>();
    }

    private void Update() {
        countdown--;

        if (countdown <= 0 && !string.IsNullOrEmpty(itemName)) {
            anim.SetTrigger("jump");
            countdown = Random.Range(100, 300);
        }
    }
}
