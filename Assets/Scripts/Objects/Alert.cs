﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alert : MonoBehaviour
{
    public enum AlertStatus { ITEMS, COOK, PLATE, OFF }
    public Station stationType;
    public PlateWorld plate;
    public SpriteRenderer needItems;
    public SpriteRenderer imgCook;

    void Start() {
        SetStatus(AlertStatus.OFF);
    }

    public void SetStatus(AlertStatus status) {
        needItems.gameObject.SetActive(false);
        plate.gameObject.SetActive(false);
        imgCook.gameObject.SetActive(false);
        gameObject.SetActive(true);

        switch (status) {
            case AlertStatus.ITEMS:
                needItems.gameObject.SetActive(true);
                break;
            case AlertStatus.COOK:
                imgCook.gameObject.SetActive(true);
                break;
            case AlertStatus.PLATE:
                plate.gameObject.SetActive(true);
                break;
            case AlertStatus.OFF:
                gameObject.SetActive(false);
                break;
        }
    }


}
