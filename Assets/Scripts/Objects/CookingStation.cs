﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class CookingStation : InteractionTrigger {
    
    public Alert alert;

    [Space(10)]
    public Minigame minigame;
    public Station stationType;

    private Meal.Recipe plate = null;
    private bool cooking = false;
    private Dictionary<Station, string> cookStrings = new Dictionary<Station, string>();
    private Meal.Recipe current = null;

    private void Start() {
        cookStrings[Station.FRYPAN] = "Press F to fry in pan";
        cookStrings[Station.KNIFE] = "Press F to chop on cutting board";
        cookStrings[Station.OVEN] = "Press F to bake in oven";
        cookStrings[Station.SOUPPOT] = "Press F to mix into soup pot";
    }

    public void DisplayNotification() {
        if (plate != null) {
            alert.SetStatus(Alert.AlertStatus.PLATE);
        } else {
            alert.SetStatus(Alert.AlertStatus.OFF);
        }
    }

    public override void Interact() {
        if (cooking) {
            // disable input while cooking
        } else if (plate != null) {
            // put plate into inventory
            if (!LevelController.Level().inventory.plate.has(plate.type)) {
                LevelController.Level().inventory.plate.AddMeal(plate.type);
                plate = null;
            }
        } else {
            LevelController.Level().OpenRecipes(this);
        }

        DisplayNotification();
    }

    public void startMinigame(Meal.Recipe recipe) {
        foreach (string item in recipe.plan) {
            LevelController.Level().inventory.removeItem(item);
        }
        current = recipe;
        cooking = true;
        if (minigame.pause) {
            LevelController.Level().Pause();
        } else {
            LevelController.Level().Unpause();
        }
        minigame.BeginGame();
        PrintMsg();
    }

    public override void PrintMsg() {
        string result = "";

        if (cooking) {
            result = "Cooking...";
        } else if (plate != null) {
            if (LevelController.Level().inventory.plate.has(plate.type)) {
                result = "Your plate already has " + plate.type;
            } else {
                result = "Press F to pick up plate";
            }
        } else {
            result = cookStrings[stationType];
        }

        LevelController.Level().messageBar.PrintMsg(result, null);
    }

    public void cook() {
        cooking = false;

        plate = current;
        current = null;

        alert.plate.ClearMeal();
        alert.plate.AddMeal(plate.type);

        LevelController.Level().meal.cookedNames.Add(plate.name);

        LevelController.Level().Unpause();
        DisplayNotification();

        if (inside) {
            PrintMsg();
        }

    }

    public void discardCurrent() {
        cooking = false;

        current = null;

        LevelController.Level().Unpause();
        DisplayNotification();
        PrintMsg();
    }
}
