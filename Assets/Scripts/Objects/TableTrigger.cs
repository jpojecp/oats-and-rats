﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableTrigger : InteractionTrigger
{
    

    public override void Interact() {
        Dictionary<MealType, bool> singlePlate = LevelController.Level().inventory.plate.clearMeal();
        PlateWorld[] plateList = LevelController.Level().plates.GetComponentsInChildren<PlateWorld>();

        // deposit plate
        foreach (KeyValuePair<MealType, bool> pair in singlePlate) {
            if (pair.Value) {
                foreach (PlateWorld tablePlate in plateList) {
                    if (!tablePlate.pm.has(pair.Key)) {
                        tablePlate.AddMeal(pair.Key);
                        LevelController.Level().meal.platedAmountByType[pair.Key]++;
                        LevelController.Level().player.playSound("plate");
                        break;
                    }
                }
            }
        }

        // check if all plates are done
        bool victory = true;
        
        foreach (PlateWorld plate in plateList) {
            if (!plate.pm.isFull()) {
                victory = false;
                break;
            }
        }

        if (victory) {
            LevelController.Level().victory("YUM!", "You cooked a good meal!", true);
        }
    }

    public override void PrintMsg() {
        string result = "";

        if (!LevelController.Level().inventory.plate.isEmpty()) {
            result = "Press F to put plate on table";
        }

        LevelController.Level().messageBar.PrintMsg(result, null);
    }

    public override bool Interactable() {
        return !LevelController.Level().inventory.plate.pm.isEmpty();
    }
}
