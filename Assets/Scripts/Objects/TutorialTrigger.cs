﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : InteractionTrigger
{
    public VictoryPage victoryPage;

    public override void Interact() {
        if (LevelController.Level().inventory.pockets.Contains("Heartberries")) {
            LevelController.Level().inventory.removeItem("Heartberries");
            /*LevelController.Level().Pause();
            victoryPage.victory("YUM!", "You found the heartberries!", true);
            victoryPage.gameObject.SetActive(true);*/
            LevelController.Level().victory("YUM!", "You found the heartberries!", true);

        }
    }

    public override void PrintMsg() {
        string result = "";
        if (LevelController.Level().inventory.pockets.Contains("Heartberries")) {
            result = "Press F to give heartberries to stranger";
        }
        LevelController.Level().messageBar.PrintMsg(result, null);
    }

    public override bool Interactable() {
        return LevelController.Level().inventory.pockets.Contains("Heartberries");
    }
}
