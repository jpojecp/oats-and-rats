﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class DelegateController : InteractionTrigger
{
    public enum Goal { FORAGE, DELIVER, HOME }

    public string characterName;
    public string animalType;
    public string favRecipe;
    public bool canDelegate;
    public TalkPage talkPage;
    public bool running;
    public Animator legs;
    public ForagePlace foragePlace;
    public GameObject homeMarkerPrefab;

    private ForageTrigger target = null;
    private AILerp ai;
    private AIDestinationSetter destination;
    private string itemName;
    private Goal goal = Goal.HOME;
    private GameObject homeMarker;
    private bool delaying = false;

    private void Start() {
        ai = GetComponent<AILerp>();
        destination = GetComponent<AIDestinationSetter>();
        homeMarker = Instantiate(homeMarkerPrefab, transform.position, Quaternion.identity);
    }

    private void Update() {
        if (running && ai.remainingDistance < 1.2) {
            if (goal == Goal.FORAGE) {
                if (string.IsNullOrEmpty(target.itemName)) {
                    goal = Goal.HOME;
                    StartCoroutine(DelayTargetSwitch(homeMarker.transform));
                } else {
                    goal = Goal.DELIVER;
                    itemName = target.itemName;
                    if (target is FarmTrigger) {
                        target.gameObject.SetActive(false);
                    }
                    StartCoroutine(DelayTargetSwitch(LevelController.Level().player.transform));
                }
            } else if (goal == Goal.DELIVER) {
                goal = Goal.HOME;
                LevelController.Level().generateItem(itemName, transform.position);
                itemName = null;
                StartCoroutine(DelayTargetSwitch(homeMarker.transform));
            } else {
                running = false;
                ai.canMove = false;
                destination.target = null;
                legs.SetBool("walking", false);
            }
        }
    }

    public override void Interact() {
        if (goal == Goal.HOME) {
            ai.canMove = false;
            running = false;
            legs.SetBool("walking", false);
            LevelController.Level().Pause();
            talkPage.SetCharacter(this);
            talkPage.gameObject.SetActive(true);
        }
    }

    public override void PrintMsg() {
        string result = "";
        if (goal != Goal.HOME) {
            result = characterName + " is busy right now";
        } else {
            result = "Press F to talk to " + characterName;
        }
        LevelController.Level().messageBar.PrintMsg(result, null);
    }

    public void Delegate() {
        running = true;
        goal = Goal.FORAGE;
        Dictionary<ForagePlace, GameObject> asdf = LevelController.Level().foragePlaces;
        ForageTrigger[] place = LevelController.Level().foragePlaces[foragePlace].GetComponentsInChildren<ForageTrigger>();
        target = place[Random.Range(0, place.Length)];
        StartCoroutine(DelayTargetSwitch(target.transform));
    }

    public IEnumerator DelayTargetSwitch(Transform t) {
        destination.target = t;
        running = false;
        ai.canMove = false;
        legs.SetBool("walking", false);

        delaying = true;
        yield return new WaitForSeconds(2);
        delaying = false;

        if (!ai.hasPath) {
            Debug.LogError("can't find path");
        }
        if (LevelController.Level().player.running) {
            Unpause();
        }
    }

    public void Pause() {
        if (!delaying) {
            running = false;
            ai.canMove = false;
            legs.SetBool("walking", false);
        }
    }

    public void Unpause() {
        if (!delaying && destination.target != null) {
            running = true;
            ai.canMove = true;
            legs.SetBool("walking", true);
        }
    }
}
