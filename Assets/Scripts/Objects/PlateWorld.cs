﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateWorld : MonoBehaviour
{
    public PlateModel pm;

    public SpriteRenderer imgContents;

    public void Awake() {
        pm = new PlateModel();
    }

    public void AddMeal(MealType type) {
        pm.addMeal(type);

        if (pm.getSprite() == null) {
            imgContents.gameObject.SetActive(false);
        } else {
            imgContents.sprite = pm.getSprite();
            imgContents.gameObject.SetActive(true);
        }
    }

    public void ClearMeal() {
        pm.clearMeal();
        imgContents.gameObject.SetActive(false);
    }

    public bool isFull() {
        return pm.isFull();
    }
}
