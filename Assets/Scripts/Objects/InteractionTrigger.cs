﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractionTrigger : MonoBehaviour
{
    public bool inside = false;


    private void OnTriggerEnter2D(Collider2D collision) {
        inside = true;
        if (collision.gameObject.CompareTag("Player")) {
            LevelController.Level().player.addTrigger(this);
            //OnEnter(LevelController.Level().player);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        inside = false;
        if (collision.gameObject.CompareTag("Player")) {
            LevelController.Level().player.removeTrigger(this);
           // OnExit(LevelController.Level().player);
        }
    }

    /*public virtual void OnEnter(PlayerController player) {

    }

    public virtual void OnExit(PlayerController player) {

    }*/

    public virtual void Interact() {

    }

    public virtual void PrintMsg() {

    }

    public virtual bool Interactable() {
        return gameObject.activeSelf;
    }
}
