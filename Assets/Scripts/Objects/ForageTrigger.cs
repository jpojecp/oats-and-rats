﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForageTrigger : InteractionTrigger {

    public string itemName;
    public SpriteRenderer imgContents;
    public string soundType = "bush";

    private void Start() {
        if (imgContents != null && string.IsNullOrEmpty(itemName)) {
            imgContents.gameObject.SetActive(false);
        }
    }

    public override void Interact() {
        if (!string.IsNullOrEmpty(itemName)) {
            if (!LevelController.Level().inventory.isFull()) {
                LevelController.Level().inventory.addItem(itemName);
                itemName = null;
                LevelController.Level().messageBar.PrintMsg("", null);
                LevelController.Level().player.playSound(soundType);
                if (imgContents != null) {
                    imgContents.gameObject.SetActive(false);
                }
            }
        }
    }

    public override void PrintMsg() {
        string result = "";
        if (!string.IsNullOrEmpty(itemName)) {
            if (LevelController.Level().inventory.isFull()) {
                result = "Inventory full. Press Q to eat an item";
            } else {
                result = "Press F to pick up " + itemName;
                LevelController.Level().messageBar.PrintMsg(result, itemName);
                return;
            }
        }

        LevelController.Level().messageBar.PrintMsg(result, null);
    }

    public override bool Interactable() {
        return !string.IsNullOrEmpty(itemName);
    }
}
