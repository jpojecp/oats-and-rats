﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketTrigger : InteractionTrigger
{
    public override void Interact() {
        LevelController.Level().inventory.addSlots(3);
        gameObject.SetActive(false);
    }
    public override void PrintMsg() {
        string result = "";

        if (gameObject.activeSelf) {
            result = "Press F to expand inventory";
        }

        LevelController.Level().messageBar.PrintMsg(result, null);
    }
}
