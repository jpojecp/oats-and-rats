﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmTrigger : ForageTrigger
{
    public override void Interact() {
        bool available = !string.IsNullOrEmpty(itemName)
            && !LevelController.Level().inventory.isFull();
        base.Interact();
        if (available) {
            gameObject.SetActive(false);
        }
    }
}
