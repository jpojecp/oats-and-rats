﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BookTrigger : InteractionTrigger
{

    public override void Interact() {
        LevelController.Level().Pause();
        LevelController.Level().OpenRecipes();
        gameObject.SetActive(false);
    }

    public override void PrintMsg() {
        if (gameObject.activeSelf) {
            LevelController.Level().messageBar.PrintMsg("Press F to open recipe book...", null);
        } else {
            LevelController.Level().messageBar.PrintMsg("", null);
        }
        
    }
}
