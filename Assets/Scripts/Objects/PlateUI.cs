﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlateUI : MonoBehaviour {

    public PlateModel pm;

    public Image imgBG;
    public Image imgContents;

    private Color colorHide = new Color(1, 1, 1, 0.5f);
    private Color colorShow = new Color(1, 1, 1, 1);

    public void Awake() {
        pm = new PlateModel();
        clearMeal();
    }

    public void AddMeal(MealType type) {
        pm.addMeal(type);

        if (pm.getSprite() == null) {
            imgContents.gameObject.SetActive(false);
        } else {
            imgContents.sprite = pm.getSprite();
            imgContents.gameObject.SetActive(true);
        }

        imgBG.color = colorShow;
    }

    public Dictionary<MealType, bool> clearMeal() {
        imgContents.gameObject.SetActive(false);

        imgBG.color = colorHide;

        return pm.clearMeal();
    }

    public bool isFull() {
        return pm.isFull();
    }

    public bool isEmpty() {
        return pm.isEmpty();
    }

    public bool has(MealType type) {
        return pm.has(type);
    }
}
