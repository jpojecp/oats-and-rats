﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenPosition : MonoBehaviour
{
    public GameObject worldObject;
    public GameObject uiObject;
    public Camera cam;

    void Start()
    {
        uiObject.transform.position = cam.WorldToScreenPoint(worldObject.transform.position);
    }
}
