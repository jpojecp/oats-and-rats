﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditsController : MonoBehaviour
{
    public TextAsset file;
    public TMPro.TextMeshProUGUI txtCredits;

    void Start()
    {
        txtCredits.text = file.text;
    }

    public void Cancel() {
        SceneManager.LoadScene("MainMenu");
    }
}
