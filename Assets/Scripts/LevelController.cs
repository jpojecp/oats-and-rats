﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Audio;

public class LevelController : MonoBehaviour {

    private static LevelController level;
    private int mealTypes = 0;
    private bool ended = false;

    [Space(10)]
    public string singleRecipe;
    public int mouths = 1;
    public bool needMain;
    public bool needSide;
    public bool needBread;
    public int mealLevel = 3;
    public bool tutorial = false;
    public bool canDelegate = false;

    [Space(10)]
    public PlayerController player;
    public InventoryController inventory;
    public Button btnRecipes;
    public MessageBar messageBar;
    public GameObject bottomBar;
    public GameObject npc;
    public GameObject forageBushes;
    public GameObject forageTrees;
    public GameObject forageRoots;
    public GameObject forageWater;
    public GameObject forageFarm;
    public GameObject stations;
    public GameObject plates;

    [Space(10)]
    public EscPage escPage;
    public RecipePage recipePage;
    public StoryPage storyPage;
    public VictoryPage victoryPage;
    public CountdownController countdown;

    [Space(10)]
    public TextAsset recipeFile;
    public Meal meal;
    public Dictionary<string, Sprite> foodImages = new Dictionary<string, Sprite>();
    public Dictionary<ForagePlace, GameObject> foragePlaces = new Dictionary<ForagePlace, GameObject>();
    public FarmTrigger itemPrefab;
    public AudioClip song;
    public List<DelegateController> npcList = new List<DelegateController>();

    public static LevelController Level() {
        return level;
    }

    void Start() {
        level = this;
        Pause();

        // start music
        if (MusicController.music() != null) {
            MusicController.music().changeSong(song);
        }

        // load food sprites
        Sprite[] foodsprites = Resources.LoadAll<Sprite>("ingredients_small");
        foreach (Sprite s in foodsprites) {
            foodImages.Add(s.name, s);
        }

        // load meal
        meal = new Meal(recipeFile, mealLevel);
        recipePage.meal = meal;
        meal.mouths = mouths;
        meal.needType[MealType.BREAD] = needBread;
        meal.needType[MealType.MAIN] = needMain;
        meal.needType[MealType.SIDE] = needSide;

        // adjust plates
        foreach (bool b in new[] { needBread, needMain, needSide }) {
            if (b) { mealTypes++; }
        }
        foreach (PlateWorld plate in plates.GetComponentsInChildren<PlateWorld>()) {
            if (plate.pm == null) {
                plate.pm = new PlateModel();
            }
            plate.pm.max = mealTypes;
        }
        if (inventory.plate.pm == null) {
            inventory.plate.pm = new PlateModel();
        }
        inventory.plate.pm.max = mealTypes;

        // set countdown timer
        countdown.timer = 1 + (mouths * mealTypes);

        // set up forage place list
        foragePlaces.Add(ForagePlace.BUSH, forageBushes);
        foragePlaces.Add(ForagePlace.TREE, forageTrees);
        foragePlaces.Add(ForagePlace.ROOT, forageRoots);
        foragePlaces.Add(ForagePlace.WATER, forageWater);
        foragePlaces.Add(ForagePlace.FARM, forageFarm);

        // populate trees, bushes with ingredients
        if (!tutorial && string.IsNullOrEmpty(singleRecipe)) {
            Populate();
        }

        // load list of npcs
        npcList = new List<DelegateController>(npc.GetComponentsInChildren<DelegateController>());

        // count liked recipes
        foreach(DelegateController del in npcList) {
            meal.recipeLikes[del.favRecipe]++;
        }

        // set up recipe page for mode
        if (!string.IsNullOrEmpty(singleRecipe)) {
            mouths = 1;
            recipePage.SetCurrent(meal.recipePlans[singleRecipe]);
            recipePage.SetMode(RecipePage.Mode.SINGLE);
        } else {
            recipePage.SetCurrent(meal.recipePlans[meal.recipeNames[0]]);
        }

        // start story page
        storyPage.gameObject.SetActive(true);
    }

    void Update() {
        if (Input.GetKeyDown("escape") && player.running) {
            Pause();
            escPage.gameObject.SetActive(true);
        }
    }

    public void Pause() {
        player.running = false;
        btnRecipes.gameObject.SetActive(false);
        bottomBar.gameObject.SetActive(false);
        countdown.PauseCountdown();
        countdown.gameObject.SetActive(false);
        foreach(DelegateController d in npcList) {
            d.Pause();
        }
    }

    public void Unpause() {
        print("unpaused");
        if (ended) { return; }
        player.running = true;
        if (!tutorial) {
            btnRecipes.gameObject.SetActive(true);
            countdown.gameObject.SetActive(true);
            if (!countdown.running) {
                countdown.startCountdown();
            } else {
                countdown.UnpauseCountdown();
            }
        }
        bottomBar.gameObject.SetActive(true);
        foreach (DelegateController d in npcList) {
            d.Unpause();
        }
    }

    public void OpenRecipes() {
        Pause();
        recipePage.SetMode(RecipePage.Mode.NAV);
        recipePage.SetCurrent(recipePage.GetCurrent());
        recipePage.gameObject.SetActive(true);
    }

    public void OpenRecipes(CookingStation st) {
        Pause();
        recipePage.currentStation = st;
        recipePage.SetMode(RecipePage.Mode.CHOOSE);
        bool hasAll = false;
        foreach (Meal.Recipe recipe in LevelController.Level().meal.recipePlansByStation[st.stationType]) {
            if (recipe.hasAll()) {
                LevelController.Level().recipePage.SetCurrent(recipe);
                hasAll = true;
                break;
            }
        }
        if (!hasAll) {
            recipePage.SetCurrent(recipePage.GetCurrent());
        }
        recipePage.gameObject.SetActive(true);
    }

    public void Populate() {
        Dictionary<ForagePlace, List<string>> ingByPlace = new Dictionary<ForagePlace, List<string>>();
        ingByPlace[ForagePlace.BUSH] = new List<string>();
        ingByPlace[ForagePlace.TREE] = new List<string>();
        ingByPlace[ForagePlace.ROOT] = new List<string>();
        ingByPlace[ForagePlace.WATER] = new List<string>();
        ingByPlace[ForagePlace.FARM] = new List<string>();

        string[] lines = recipeFile.text.Split('\n');
        string[] ingredients = lines[0].Split(',');
        string[] places = lines[2].Split(',');
        string[] mLevels = lines[3].Split(',');

        for (int i = 5; i < ingredients.Length; i++) {
            int mLevel = int.Parse(mLevels[i]);
            if (mLevel <= mealLevel && !string.IsNullOrEmpty(places[i])) {
                ForagePlace place = (ForagePlace) System.Enum.Parse(typeof(ForagePlace), places[i]);
                ingByPlace[place].Add(ingredients[i]);
            }
        }

        foreach(KeyValuePair<ForagePlace, GameObject> pair in foragePlaces) {
            if (pair.Key == ForagePlace.FARM) {
                break;
            }
            if (ingByPlace[pair.Key].Count > 0) {
                ForageTrigger[] triggers = pair.Value.GetComponentsInChildren<ForageTrigger>();
                List<ForageTrigger> triggerList = new List<ForageTrigger>(triggers);
                Shuffler.Shuffle<ForageTrigger>(triggerList);
                int amount = triggers.Length / ingByPlace[pair.Key].Count;
                int i = 0;
                int j = 0;
                foreach (ForageTrigger t in triggerList) {
                    // god this code is so dumb
                    // TODO MAKE THIS BETTER
                    if (i == ingByPlace[pair.Key].Count) {
                        break;
                    }
                    t.itemName = ingByPlace[pair.Key][i];
                    if (t.imgContents != null) {
                        t.imgContents.gameObject.SetActive(true);
                    }
                    j++;
                    if (j == amount) {
                        j = 0;
                        i++;
                    }
                }
            }
        }
    }

    public void generateItem(string itemName, Vector3 position) {
        FarmTrigger item = Instantiate(itemPrefab, position, Quaternion.identity);
        item.itemName = itemName;
        item.GetComponent<SpriteRenderer>().sprite = foodImages[itemName];
    }

    public void victory(string title, string msg, bool win) {
        ended = true;
        Pause();
        victoryPage.victory(title, msg, win);
        victoryPage.gameObject.SetActive(true);
        victoryPage.victoryMusic(win);
    }
}
