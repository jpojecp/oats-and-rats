﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class InventoryController : MonoBehaviour {

    private List<Image> pocketImages = new List<Image>();
    private int max = 0;

    public int amount = 3;
    public List<string> pockets = new List<string>();
    public PlateUI plate;
    public Image prefabPocketSlot;

    void Start() {
        addSlots(amount);
    }

    public void addItem(string item) {
        if (!isFull()) {
            pockets.Add(item);
            display();
        }
    }

    public void removeItem(string item) {
        pockets.Remove(item);
        display();
    }

    public void addPlate(MealType type) {
        plate.AddMeal(type);
    }

    public void removePlate() {
        plate.clearMeal();
    }

    public void display() {
        for (int i = 0; i < max; i++) {
            if (pockets.ElementAtOrDefault(i) == null) {
                pocketImages[i].gameObject.SetActive(false);
            } else {
                pocketImages[i].sprite = LevelController.Level().foodImages[pockets[i]];
                pocketImages[i].gameObject.SetActive(true);
            }
        }
    }

    public bool isFull() {
        return pockets.Count >= max;
    }

    public void addSlot() {
        Image img = Instantiate(prefabPocketSlot, Vector3.zero, Quaternion.identity);
        img.transform.SetParent(gameObject.transform, false);
        pocketImages.Add(img.transform.GetChild(0).GetComponent<Image>());
        max++;
    }

    public void addSlots(int amount) {
        for (int i = 0; i < amount; i++) {
            addSlot();
        }
    }
}
