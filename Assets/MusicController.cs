﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicController : MonoBehaviour
{
    private static MusicController instance;
    private AudioSource src;

    void Awake()
    {
        if (MusicController.music() == null) {
            instance = this;
            src = GetComponent<AudioSource>();
            DontDestroyOnLoad(this);
        } else {
            Destroy(this);
        }
    }

    public static MusicController music() {
        return instance;
    }

    public void changeSong(AudioClip clip) {
        if (src.clip != clip) {
            src.Stop();
            src.clip = clip;
            src.Play();
        }
    }

    public void fadeSong() {
        src.Stop();
        src.clip = null;
    }
}
